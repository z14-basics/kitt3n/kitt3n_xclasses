<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (class_exists('TYPO3\\CMS\\Frontend\\Middleware\\ShortcutAndMountPointRedirect')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\Middleware\ShortcutAndMountPointRedirect::class] = [
        'className' => \KITT3N\Kitt3nXclasses\Middleware\ShortcutAndMountPointRedirect::class,
    ];
}